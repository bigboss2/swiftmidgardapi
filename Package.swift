// swift-tools-version:5.3
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "SwiftMidgardApi",
    platforms: [
        .macOS(.v10_12),
        .iOS(.v13),
            .watchOS(.v3)
        ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "SwiftMidgardApi",
            targets: ["SwiftMidgardApi"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(url: "https://github.com/Moya/Moya.git", .upToNextMajor(from: "14.0.0")),
        .package(url: "https://github.com/attaswift/BigInt.git", from: "5.2.0"),
        .package(name: "BigNumber", url: "https://github.com/mkrd/Swift-BigInt.git", from: "2.1.1"),
        .package(url: "https://github.com/mxcl/PromiseKit", from: "6.8.0"),
        .package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", from: "4.0.0")
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
                   name: "SwiftMidgardApi",
                   dependencies: ["Moya", "BigInt", "PromiseKit", "BigNumber", "SwiftyJSON"],
                   resources: [.process("txs.json")]
                ),
        .testTarget(
            name: "SwiftMidgardApiTests",
            dependencies: ["SwiftMidgardApi"]),
    ]
)
