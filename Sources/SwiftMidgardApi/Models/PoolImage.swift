//
//  File.swift
//  
//
//  Created by Bigboss on 18.04.21.
//

import Foundation


public struct PoolImage: Codable {

    public var asset: String
    public var url: String
    public var status: String

}

public extension PoolImage {
    var isPending: Bool {
        return status == "Staged"
    }
}
