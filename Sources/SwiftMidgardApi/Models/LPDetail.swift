//
//  File.swift
//  
//
//  Created by BigBoss on 22.05.21.
//

import Foundation

struct LPDetail {
    var redeemable: AssetPair
    var feeEarn: AssetPair
    var addedAmount: AssetPair
    var withdrawnAmount: AssetPair
    var gainLoss: AssetPair
    var protection: AssetPair
    var asset: String
}

struct AssetPair {
    var rune: String
    var asset: String
    var isRuneNegative: Bool = false
    var isAssetNegative: Bool = false
}


extension AssetPair {
    static func createEmpty() -> AssetPair{
        return AssetPair(rune: "0.00", asset: "0.00")
    }
}
