//
//  File.swift
//  
//
//  Created by Bigboss on 29.08.21.
//

import Foundation
import SwiftyJSON
import BigNumber

public struct WalletBalance: Codable, Identifiable {
    public var chain: Chain
    public var items: [WalletItem]
    public var id: UUID = UUID()
}

public struct WalletItem: Codable, CustomStringConvertible {
    public var name: String
    public var assetImg: String
    public var geckoId: String
    public var symbol: String
    public var amount: BDouble
    public var confirmBalance: BDouble
    public var unconfimBalance: BDouble
    public var price: BDouble?
    public var usdAmount: BDouble {
        return (price ?? BDouble(0)) * self.amount
    }
    
    public var description: String {
        return "\nname: \(name), amount: \(amount.asBaseNumber), symbol: \(symbol), price: \(price?.asValue), usdAmount: \(usdAmount.asBaseNumber) url: \(assetImg)"
    }
}

extension WalletBalance {
    static func decodeBalace(data: Data, chain: Chain) -> Swift.Result<WalletBalance, NetworkingError> {
        print("---- custom decode data:")
        do {
            let json = try JSON(data: data)
            var items: [WalletItem] = []
            switch chain {
            case .thorchain:
                items = json.decodeThorchainBalance()
            case .binance:
                items = json.decodeBinanceBalance()
            case .litecoin:
                items = json.decodeLitecoinBalance()
            case .ethereum:
                items = json.decodeEthBalance()
            default:
                items = json.decodeOtherBalance(name: chain.name, img: chain.url, geckoId: chain.symbolId.id, symbol: chain.symbolId.symbol)
            }
            return .success(WalletBalance(chain: chain, items: items))
        }catch{
            return .success(WalletBalance(chain: .unknown, items: []))
        }
        
    }
}

extension JSON {
    
    func decodeEthBalance() -> [WalletItem]{
        
        var items = self["tokens"].array?.compactMap({ json -> WalletItem? in
            guard let name = json["tokenInfo"]["name"].string,
                  let decimalString = json["tokenInfo"]["decimals"].string,
                  let decimal = Double(decimalString),
                  let amount = json["rawBalance"].string,
                  let symbol = json["tokenInfo"]["symbol"].string,
                  let bAmount = BDouble(amount) else {
                return nil
            }
            
            let baseValue = BDouble(pow(10, decimal))
            let realAmount = bAmount / baseValue
            let url = json["tokenInfo"]["image"].string
            let price = json["tokenInfo"]["price"]["rate"].double ?? 0
            let geckoId = json["tokenInfo"]["coingecko"].string ?? ""
            return WalletItem( name: name, assetImg: url != nil ? "https://ethplorer.io\(url!)" : "", geckoId: geckoId, symbol: symbol,
                               amount: realAmount * BDouble.base,
                               confirmBalance: realAmount * BDouble.base,
                               unconfimBalance: BDouble(0),
                               price: BDouble(price))
            
        })
        
        if let eth = self["ETH"]["rawBalance"].string,
           let price = self["ETH"]["price"]["rate"].double,
           let ethAmount = BDouble(eth) {
            let item = WalletItem(name: "Ethereum", assetImg: "https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/ethereum/info/logo.png",
                                  geckoId: Chain.ethereum.symbolId.id,
                                  symbol: Chain.ethereum.symbolId.symbol,
                                  amount: ethAmount / BDouble(10000000000),
                                  confirmBalance: ethAmount / BDouble(10000000000) ,
                                  unconfimBalance: BDouble(0),
                                  price: BDouble(price))
            items = [item] + (items ?? [])
        }
        
        
        return items ?? []
    }
    
    func decodeThorchainBalance() -> [WalletItem]{
        let items  = self["result"].array?.compactMap { json -> WalletItem? in
            guard let name = json["denom"].string,
                  let amount = json["amount"].string,
                  let bAmount = BDouble(amount) else {
                return nil
            }
            return WalletItem( name: "THORChain", assetImg: Chain.thorchain.url, geckoId: Chain.thorchain.symbolId.id, symbol: Chain.thorchain.symbolId.symbol, amount: bAmount, confirmBalance: bAmount , unconfimBalance: BDouble(0))
        }
        return items ?? []
    }
    
    func decodeBinanceBalance() -> [WalletItem]{
        let items  = self["balance"].array?.compactMap { json -> WalletItem? in
            guard let name = json["assetName"].string,
                  let amount = json["free"].double,
                  let assetImg = json["assetImg"].string,
                  let id = json["asset"].string,
                  let symbol = json["mappedAsset"].string,
                  let price = json["assetPrice"].double else {
                return nil
            }
            
            return WalletItem(name: name, assetImg: id == "ETH-1C9" ? "https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/ethereum/info/logo.png" : assetImg, geckoId: id, symbol: symbol, amount: BDouble(amount) * BDouble.base, confirmBalance: BDouble(amount) * BDouble.base , unconfimBalance: BDouble(0), price: BDouble(price))
        }
        return items ?? []
    }
    
    func decodeLitecoinBalance() -> [WalletItem] {
       guard let confirmBalance  = self["data"]["confirmed_balance"].string,
             let unConfirmBalance = self["data"]["unconfirmed_balance"].string,
             let bConfirm = BDouble(confirmBalance),
             let bUnconfirm = BDouble(unConfirmBalance) else {return []}
        return [WalletItem(name: "Litecoin", assetImg: Chain.litecoin.url, geckoId: Chain.litecoin.symbolId.id, symbol: Chain.litecoin.symbolId.symbol, amount: (bConfirm + bUnconfirm) * BDouble.base, confirmBalance: bConfirm, unconfimBalance: bUnconfirm)]
    }
    
    // / 100000000
    func decodeOtherBalance(name: String, img: String, geckoId: String, symbol: String) -> [WalletItem] {
        guard let confirmBalance  = self["confirmed"].double,
              let unConfirmBalance = self["unconfirmed"].double else {return []}
        print(BDouble(confirmBalance).asBaseNumber)
         return [WalletItem(name: name, assetImg: img, geckoId: geckoId, symbol: symbol, amount: BDouble(confirmBalance) + BDouble(unConfirmBalance), confirmBalance: BDouble(confirmBalance), unconfimBalance: BDouble(unConfirmBalance))]
    }
}
