//
//  File.swift
//  
//
//  Created by BigBoss on 9/12/20.
//

import Foundation

public struct ErrorResponse: Codable {
    public var message: String
}
