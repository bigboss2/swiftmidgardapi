//
//  File.swift
//  
//
//  Created by BigBoss on 10/4/20.
//

import Foundation
import BigInt

public struct StakerOriginal: CustomStringConvertible {
    public var pool: String
    public var totalAssetStake: BigInt
    public var totalTargetStake: BigInt
    public var totalWithdrawRatio: Int
    
    public var description: String {
        return "pool :  \(pool) totalAssetStake : \(totalAssetStake.amoutString) totalTargetStake : \(totalTargetStake.amoutString) totalWithdrawRatio : \(totalWithdrawRatio) "
    }
}

extension StakerOriginal {
    public static func create(stakeTxs: [StakerTranscation], pool: String) -> StakerOriginal {
        var totalAssetStake: BigInt = BigInt("0")
        var totalTargetStake: BigInt = BigInt("0")
        var withdrawRatio: Int = 0
        print("----------\n")
        stakeTxs.forEach{
            switch $0.type {
            case .stake:
                totalAssetStake = totalAssetStake + $0.assetAmount
                totalTargetStake = totalTargetStake + $0.targetAmount
                
                print("pool: \(pool) stake totalTargetStake : \(totalTargetStake.amoutString) totalAssetStake : \(totalAssetStake.amoutString)")
            case .unstake:
                if let norNum = Int($0.ratio),
                   let ratiobigNum = BigInt($0.ratio),
                   norNum > 0 {
                    let ration = Double(Double(norNum)/10000.0)
                    print(ration)
                    print(totalAssetStake.amoutString)
                    let assetRemove = (totalAssetStake * ratiobigNum ) / 10000
                    totalAssetStake = totalAssetStake - assetRemove
                    
                    let targetRemove =  (totalTargetStake * ratiobigNum ) / 10000
                    totalTargetStake = totalTargetStake - targetRemove
                    if (norNum == 10000) {
                        withdrawRatio = 0
                    }else{
                        withdrawRatio = withdrawRatio + norNum
                    }
                    print("pool: \(pool) unstake after targetRemove: -\(targetRemove.amoutString) totalTargetStake : \(totalTargetStake.amoutString) assetRemove: -\(assetRemove.amoutString) totalAssetStake : \(totalAssetStake.amoutString) \(ratiobigNum)*")
                }
            default:
                break
            }
        }
        print("\n----------")
        
        return StakerOriginal.init(pool: pool, totalAssetStake: totalAssetStake, totalTargetStake: totalTargetStake, totalWithdrawRatio: withdrawRatio)
    }
}
