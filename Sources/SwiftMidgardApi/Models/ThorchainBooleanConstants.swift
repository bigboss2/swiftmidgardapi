//
// ThorchainBooleanConstants.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct ThorchainBooleanConstants: Codable {

    public var strictBondStakeRatio: Bool?

    public init(strictBondStakeRatio: Bool?) {
        self.strictBondStakeRatio = strictBondStakeRatio
    }

    public enum CodingKeys: String, CodingKey { 
        case strictBondStakeRatio = "StrictBondStakeRatio"
    }

}

