//
// BondMetrics.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation



public struct BondMetrics: Codable {

    /** Total bond of active nodes */
    public var totalActiveBond: String?
    /** Average bond of active nodes */
    public var averageActiveBond: String?
    /** Median bond of active nodes */
    public var medianActiveBond: String?
    /** Minumum bond of active nodes */
    public var minimumActiveBond: String?
    /** Maxinum bond of active nodes */
    public var maximumActiveBond: String?
    /** Total bond of standby nodes */
    public var totalStandbyBond: String?
    /** Average bond of standby nodes */
    public var averageStandbyBond: String?
    /** Median bond of standby nodes */
    public var medianStandbyBond: String?
    /** Minumum bond of standby nodes */
    public var minimumStandbyBond: String?
    /** Maximum bond of standby nodes */
    public var maximumStandbyBond: String?

    public init(totalActiveBond: String?, averageActiveBond: String?, medianActiveBond: String?, minimumActiveBond: String?, maximumActiveBond: String?, totalStandbyBond: String?, averageStandbyBond: String?, medianStandbyBond: String?, minimumStandbyBond: String?, maximumStandbyBond: String?) {
        self.totalActiveBond = totalActiveBond
        self.averageActiveBond = averageActiveBond
        self.medianActiveBond = medianActiveBond
        self.minimumActiveBond = minimumActiveBond
        self.maximumActiveBond = maximumActiveBond
        self.totalStandbyBond = totalStandbyBond
        self.averageStandbyBond = averageStandbyBond
        self.medianStandbyBond = medianStandbyBond
        self.minimumStandbyBond = minimumStandbyBond
        self.maximumStandbyBond = maximumStandbyBond
    }


}

