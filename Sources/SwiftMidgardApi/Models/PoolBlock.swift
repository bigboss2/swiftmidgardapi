//
//  File.swift
//  
//
//  Created by BigBoss on 10/8/20.
//

import Foundation

public struct PoolBlock: Codable {
//    public var balance_rune: String
//    public var balance_asset: String
//    public var asset: String
//    public var pool_units: String
//    public var status: String
//    public var time: String
//    public var height: String
    public let asset : String
    public       let balanceAsset : String
    public      let balanceRune : String
    public       let height : Int
    public      let poolUnits : String
    public      let status : String
    public      let time : String

          enum CodingKeys: String, CodingKey {
                  case asset = "asset"
                  case balanceAsset = "balance_asset"
                  case balanceRune = "balance_rune"
                  case height = "height"
                  case poolUnits = "pool_units"
                  case status = "status"
                  case time = "time"
          }
      
    public init(from decoder: Decoder) throws {
                  let values = try decoder.container(keyedBy: CodingKeys.self)
                  asset = try values.decode(String.self, forKey: .asset)
                  balanceAsset = try values.decode(String.self, forKey: .balanceAsset)
                  balanceRune = try values.decode(String.self, forKey: .balanceRune)
                  height = try values.decode(Int.self, forKey: .height)
                  poolUnits = try values.decode(String.self, forKey: .poolUnits)
                  status = try values.decode(String.self, forKey: .status)
                  time = try values.decode(String.self, forKey: .time)
          }
}



//interface PoolBlock {
//    balance_rune?: string;
//    balance_asset?: string;
//    asset?: string;
//    pool_units?: string;
//    status?: string;
//    time?: string;
//    height: number;
//}
