//
//  StakerTransaction.swift
//  ExplorerSwift
//
//  Created by BigBoss on 10/4/20.
//

import Foundation
import BigInt



public struct StakerTranscation {
    typealias AmountDetail = (asset: String, target: String, assetAmount: BigInt, targetAmount: BigInt)

    public var pool: String
    public var  asset: String
    public var  target: String
    public var  stakeUnit: BigInt
    public var  assetAmount: BigInt
    public var  targetAmount: BigInt
    public var  type: TxDetails.TxType
    public var  memo: String
    public var  status: TxDetails.Status
    public var   date: Int64
    public var address: String
    public var tx: String
    public var height: String
    public var withdrawratio: Int?
    
    public init(
        pool: String,
      asset: String,
      target: String,
      stakeUnit: BigInt,
      assetAmount: BigInt,
      targetAmount: BigInt,
      type: TxDetails.TxType,
      memo: String,
      status: TxDetails.Status,
       date: Int64,
        address: String,
        tx: String,
        height: String,
        withdrawratio: Int? = nil){
        self.pool = pool
        self.asset = asset
        self.target = target
        self.stakeUnit = stakeUnit
        self.assetAmount = assetAmount
        self.targetAmount = targetAmount
        self.type = type
        self.memo = memo
        self.status = status
        self.date = date
        self.address = address
        self.tx = tx
        self.height = height
        self.withdrawratio = withdrawratio
    }
    
    public static func extractRation(memo: String) -> Int?{
        let str = memo.components(separatedBy: ":")
        if str.count == 3 {
            return Int(str[2])
        }else{
            return nil
        }
    }
    
    public static func create(txs: TxDetails) -> StakerTranscation?{
        let date1 = NSDate(timeIntervalSince1970: Double(txs.date))
        print("date: \(date1)  \(txs.type?.rawValue)  \(txs._in?.memo) "  )
//        if (TxDetails.Status.pending.rawValue == txs.status?.rawValue) {
//            return nil
//        }
        guard let pool = txs.pool,
              let stakeUnit = txs.events?.stakeUnits,
              let stakeUnitBN = BigInt(stakeUnit),
              let memo = txs._in?.memo,
              let status = txs.status,
              let type = txs.type,
              let tx = txs._in?.txID,
              let address = txs._in?.address,
              let state = txs.status?.rawValue,
              let height = txs.height
        else {
            fatalError("error")
            return nil
        }
        let date = txs.date
        var amountDetial:AmountDetail? = nil
//        let ratio =
        
        switch txs.type {
        case .stake:
            amountDetial =  self.getStakeTransaction(txDetail: txs)
        case .unstake:
            amountDetial = self.getUnstakeTransaction(txDetail: txs)
        default:
            break
        }
        
        guard let amountDetail = amountDetial else {
            return nil
        }
        
        return StakerTranscation.init(pool: pool, asset: amountDetail.asset, target:  amountDetail.target, stakeUnit: stakeUnitBN, assetAmount:  amountDetail.assetAmount, targetAmount:  amountDetail.targetAmount, type: type, memo: memo, status: status, date: date, address: address, tx: tx, height: height, withdrawratio: StakerTranscation.extractRation(memo: memo))
    
    }
    
    static func getStakeTransaction(txDetail: TxDetails) -> (asset: String, target: String, assetAmount: BigInt, targetAmount: BigInt)?{
       
        var asset: String = ""
        var target: String = "BNB.RUNE-B1A"
        var assetAmount = BigInt("0")
        var targetAmount = BigInt("0")
        
        txDetail._in?.coins?.forEach{
            if $0.asset != "BNB.RUNE-B1A" {
                asset = $0.asset ?? ""
                assetAmount = $0.amount ?? BigInt("0")
            }else{
                target = "BNB.RUNE-B1A"
                targetAmount = $0.amount ?? BigInt("0")
            }
        }
        
        return (asset, target,assetAmount, targetAmount)
        
    }
    
    static func getUnstakeTransaction(txDetail: TxDetails) -> (asset: String, target: String, assetAmount: BigInt, targetAmount: BigInt)?{
        var asset: String = ""
        var target: String = "BNB.RUNE-B1A"
        var assetAmount = BigInt("0")
        var targetAmount = BigInt("0")

        txDetail.out?.forEach{
            guard let coin = $0.coins?.first,
                  let asset1 = coin.asset else { return}
            if coin.asset != "BNB.RUNE-B1A" {
                asset = asset1
                assetAmount = coin.amount ?? BigInt("0")
            }else{
                target = "BNB.RUNE-B1A"
                targetAmount = coin.amount ?? BigInt("0")
            }
        }
        return (asset, target,assetAmount, targetAmount)
    }
    
    
}

extension StakerTranscation {
    public var ratio: String {
//        WITHDRAW:BNB.BNB:10000
        let ratio = self.memo.components(separatedBy: ":")
        if ratio.count == 3 {
            return ratio[2]
        }else{
            return "0"
        }
    }
}
