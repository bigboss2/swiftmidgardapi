//
//  File.swift
//  
//
//  Created by Bigboss on 03.10.21.
//

import Foundation
import BigNumber
import SwiftyJSON

public struct CoinGecko: Codable, CustomStringConvertible {
    public let symbol: String
    public let price: BDouble
    public let history: [Double]
    
    static func decode(data: Data) -> Swift.Result<[CoinGecko], NetworkingError> {
        do {
            let json = try JSON(data: data)
            let items =  json.array?.compactMap { item -> CoinGecko? in
                if let symbol = item["symbol"].string,
                   let price = item["current_price"].double {
                    let histroy = item["sparkline_in_7d"]["price"].arrayObject as? [Double] ?? []
                    return CoinGecko(symbol: symbol, price: BDouble(price), history: histroy)
                }else {return nil}
            }
            return .success(items ?? [])
        } catch {
            return .failure(NetworkingError.apiFailed(code: 400, message: "Failed"))
        }
        
    }
    
    public var description: String {
        return "\nsymbol: \(symbol), price: \(price.asValue)"
    }
}
