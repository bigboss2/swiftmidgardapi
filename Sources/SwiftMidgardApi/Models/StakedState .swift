//
//  StakedState .swift
//  Model Generated using http://www.jsoncafe.com/ 
//  Created on January 5, 2021

import Foundation
import BigNumber
import BigInt

public struct StakedState: Codable {

    public  let assetstake : String
    public   let assetwithdrawn : String
    public   let firststake : Double
    public    let laststake : Double
    public    let pool : String
    public    let poolunits : String
    public    let runestake : String
    public   let runewithdrawn : String
    public    let totalstakedasset : String
    public    let totalstakedrune : String
    public    let totalstakedusd : String
    public    let totalunstakedasset : String
    public    let totalunstakedrune : String
    public    let totalunstakedusd : String
    public let postrunestake: String
    public let postassetstake: String

}

extension StakedState {
    static func createEmpty() -> StakedState{
        return StakedState(assetstake: "", assetwithdrawn: "", firststake: 0, laststake: 0, pool: "", poolunits: "", runestake: "", runewithdrawn: "", totalstakedasset: "", totalstakedrune: "", totalstakedusd: "", totalunstakedasset: "", totalunstakedrune: "", totalunstakedusd: "", postrunestake: "", postassetstake: "")
    }
}

public extension StakedState {
    var postrunestakeBN: BDouble {
        return postrunestake.asBDoule
    }
    
    var postassetstakeBN: BDouble {
        return postassetstake.asBDoule
    }
    
}

public extension String {
    var asBDoule: BDouble {
        return BDouble(self) ?? 0
    }
}

public extension BigInt {
    var asBDoule: BDouble {
        return BDouble(exactly: self) ?? 0
    }
}
