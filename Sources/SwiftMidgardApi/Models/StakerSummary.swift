//
//  File.swift
//  
//
//  Created by Bigboss on 05.01.21.
//

import Foundation
import BigInt
import BigNumber

public struct StakerSummary {
    public let address: String
    public let pools: [String]
    public let stakedStates: [StakedState]
    public let poolDetails: [PoolDetail]
    public let poolShares: [StakerPoolShare]
    public var lpVsHodls: [LPvsHodl] = []
    public var totallpVsHodl: LPvsHodl = LPvsHodl()
    public var gainLoss: [GainLoss]  = []
    public var totalGainLoss: GainLoss = GainLoss()
    public var runePriceUSD: BDouble = 0 
    
    public init(address: String = "",
         pools: [String] = [],
         stakedStates: [StakedState] = [],
         poolDetails: [PoolDetail] = [],
         runePriceUSD: String = "0.0") {
        self.address = address
        self.pools = pools
        self.stakedStates = stakedStates
        self.poolDetails = poolDetails
        self.poolShares = Self.calculate(stakedStates: stakedStates, poolDetails: poolDetails, runeUSDPrice: runePriceUSD)
        self.runePriceUSD = BDouble(runePriceUSD) ?? 0
    }
    
    public mutating func calculateLPvsHODL(){
        var lp: BDouble = 0
        var percent: Decimal = 0
        var _: Decimal = 0
        stakedStates.forEach{ state in
            let poolShare = self.poolShares.filter{$0.asset == state.pool}.first!
            let poolDetail = self.poolDetails.filter{$0.asset == state.pool}.first!
            let lpVsHodl = LPvsHodl.init(stakedState: state, poolShare: poolShare, poolDetail: poolDetail)
            self.lpVsHodls.append(lpVsHodl)
            lp = lp + lpVsHodl.value
            percent = percent + lpVsHodl.percentage
        }
        print(lp)
        print(percent)
        let total = LPvsHodl(value: lp, percent: percent / Decimal(stakedStates.count))
        self.totallpVsHodl = total
    }
    
    public mutating func calcuateGainLoss(){
        var value: BDouble = 0
        var percent: BDouble = 0
        stakedStates.forEach{ state in
            let poolShare = self.poolShares.filter{$0.asset == state.pool}.first!
            let gainLoss = GainLoss(stakedState: state, poolShare: poolShare)
            self.gainLoss.append(gainLoss)
            value = value + gainLoss.value
            percent = percent + gainLoss.percentage
        }
        percent = percent / BDouble(stakedStates.count)
        self.totalGainLoss = GainLoss(value: value, percent: percent)
        print(self.totalGainLoss.value.isNegative())
        print("total G/L : \(value.asBaseValue)")
        print("total % : \(self.totalGainLoss.percentageString)")
    }
    
    public static func calculate(stakedStates: [StakedState], poolDetails: [PoolDetail], runeUSDPrice: String) -> [StakerPoolShare]{
        var poolShares: [StakerPoolShare] = []
        stakedStates.forEach{ state in
            guard let poolDetail = poolDetails.filter({$0.asset == state.pool}).first else {fatalError("\(state.pool) pool not found")}
            let poolshare  = StakerPoolShare.init(poolDetail: poolDetail, stakeUnit: state.poolunits, runeUSDPrice: runeUSDPrice)
            print("-----------")
            poolShares.append(poolshare)
            print(poolshare)
        }
        return poolShares
    }
    
}

public extension StakerSummary {
    var totalValue: String {
        let total = self.poolShares.reduce(0) { $0 + $1.totalValue}
        return total.asBaseValue()
    }
    
    func itemAtIndex(index: Int, poolImage:@escaping (String)-> String, feeEearn: @escaping (String)-> ReturnMetrics) -> LPStateDetail {
        let pool = pools[index]
        let stakedAsset = self.stakedStates[index]
        let poolShare = self.poolShares[index]
        let lpVsHodl = self.lpVsHodls[index]
        let gainLoss = self.gainLoss[index]
        let poolDetail = self.poolDetails[index]
        print(poolImage)
        return LPStateDetail(address: self.address,
                             pool: pool,
                             stakedStates: stakedAsset,
                             poolShare: poolShare,
                             lpVsHodl: lpVsHodl,
                             gainLoss: gainLoss,
                             runePriceUSD: self.runePriceUSD,
                             assetPriceUSD: poolDetail.assetPriceUSDBN,
                             feeEarn: feeEearn(poolShare.asset),
                             assetUrl: poolImage(poolShare.asset))
        
    }
}

