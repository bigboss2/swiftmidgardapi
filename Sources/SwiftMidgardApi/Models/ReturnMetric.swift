//
//  File.swift
//  
//
//  Created by Bigboss on 09.05.21.
//

import Foundation
import UIKit

public struct ReturnMetrics {
    public var hodleReturn: Double = 0
    public var netReturn: Double = 0
    public var uniswapReturn: Double = 0
    public var impLoss: Double = 0
    public var percentage: Double = 0
    public var fees: Double = 0
    public var runeFee: Double = 0
    public var assetFee: Double = 0
    public var asset: String = ""
}

public extension ReturnMetrics {
    
    static var emptyFee: ReturnMetrics {
        return ReturnMetrics()
    }
    
    static func combineReturnMetric(position: [LPPositions]) -> ReturnMetrics {
        var metric = ReturnMetrics()
        position.forEach{
            let temp = ReturnMetrics.makeReturnMetric(snapshots: $0.list)
            metric.hodleReturn = metric.hodleReturn + temp.hodleReturn
            metric.netReturn = metric.netReturn + temp.netReturn
            metric.uniswapReturn = metric.uniswapReturn + temp.uniswapReturn
            metric.fees = metric.fees + temp.fees
            metric.runeFee = metric.runeFee + temp.runeFee
            metric.assetFee = metric.assetFee + temp.assetFee
            metric.percentage = metric.percentage + temp.percentage
            metric.asset = temp.asset
        }
        return metric
    }
    
    static func makeReturnMetric(snapshots: [LPPosition]) -> ReturnMetrics {
        var hodlReturn = 0.0
        var netReturn = 0.0
        var uniswapReturn = 0.0
        var fees = 0.0
        var impLoss = 0.0
        var percent = 0.0

        for i in 0 ..< snapshots.count - 1 {
            // get positions at both bounds of the window
            let positionT0 = snapshots[i]
            let positionT1 = snapshots[i+1]

            let results = getMetricsForPositionWindow(positionT0: positionT0, positionT1: positionT1)
            hodlReturn = hodlReturn + results.hodleReturn
            netReturn = netReturn + results.netReturn
            uniswapReturn = uniswapReturn + results.uniswapReturn
            fees = fees + results.fees
            impLoss = impLoss + results.impLoss
            percent = percent + results.percentage
        }
        var metric = ReturnMetrics()
        metric.hodleReturn = hodlReturn
        metric.netReturn = netReturn
        metric.uniswapReturn = uniswapReturn
        metric.impLoss = impLoss
        metric.percentage = percent
        metric.fees = fees
        
        
        let lastest = snapshots[snapshots.count - 1]
        metric.runeFee = (fees / 2) / lastest.token0PriceUSD
        metric.assetFee = (fees / 2) / lastest.token1PriceUSD
        metric.asset = lastest.pair
        print(metric.runeFee.asValue(asset: "RUNE"))
        print(metric.assetFee.asValue(asset: StakerPoolShare.assetName(asset: lastest.pair)))
        return metric
    }
    
    static func getMetricsForPositionWindow(positionT0: LPPosition, positionT1: LPPosition) -> ReturnMetrics {
        // calculate ownership at ends of window, for end of window we need original LP token balance / new total supply
        let t0Ownership = positionT0.liquidityTokenBalance / positionT0.liquidityTokenTotalSupply
        let t1Ownership = positionT0.liquidityTokenBalance / positionT1.liquidityTokenTotalSupply
        
        // get starting amounts of token0 and token1 deposited by LP
        let token0_amount_t0 = t0Ownership * positionT0.reserve0
        let token1_amount_t0 = t0Ownership * positionT0.reserve1
        
        // get current token values
        let token0_amount_t1 = t1Ownership * positionT1.reserve0
        let token1_amount_t1 = t1Ownership * positionT1.reserve1
        
        // calculate squares to find imp loss and fee differences
        let sqrK_t0 = sqrt(token0_amount_t0 * token1_amount_t0)
        // eslint-disable-next-line eqeqeq
        let priceRatioT1 = positionT1.token0PriceUSD != 0 ? positionT1.token1PriceUSD / positionT1.token0PriceUSD : 0
        
        let token0_amount_no_fees = priceRatioT1 != 0 ? sqrK_t0 * sqrt(priceRatioT1) : 0
        let token1_amount_no_fees = priceRatioT1 != 0 ? sqrK_t0 / sqrt(priceRatioT1) : 0
        let no_fees_usd =
            token0_amount_no_fees * positionT1.token0PriceUSD + token1_amount_no_fees * positionT1.token1PriceUSD
        
        let difference_fees_token0 = token0_amount_t1 - token0_amount_no_fees
        let difference_fees_token1 = token1_amount_t1 - token1_amount_no_fees
        let difference_fees_usd =
            difference_fees_token0 * positionT1.token0PriceUSD + difference_fees_token1 * positionT1.token1PriceUSD
        
        // calculate USD letue at t0 and t1 using initial token deposit amounts for asset return
        let assetValueT0 = token0_amount_t0 * positionT0.token0PriceUSD + token1_amount_t0 * positionT0.token1PriceUSD
        let assetValueT1 = token0_amount_t0 * positionT1.token0PriceUSD + token1_amount_t0 * positionT1.token1PriceUSD
        
        let imp_loss_usd = no_fees_usd - assetValueT1
        let uniswap_return = difference_fees_usd + imp_loss_usd
        
        // get net value change for combined data
        let netValueT0 = t0Ownership * positionT0.reserveUSD
        let netValueT1 = t1Ownership * positionT1.reserveUSD
        
        let percentage =  imp_loss_usd / ((positionT0.token0PriceUSD * token0_amount_t1) + (positionT0.token1PriceUSD * token1_amount_t0))
        
        var metric = ReturnMetrics()
        metric.hodleReturn = assetValueT1 - assetValueT0
        metric.netReturn = netValueT1 - netValueT0
        metric.uniswapReturn = uniswap_return
        metric.impLoss = imp_loss_usd
        metric.percentage = percentage
        metric.fees = difference_fees_usd
        
        return metric
        
    }
}
