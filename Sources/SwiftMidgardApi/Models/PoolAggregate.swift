//
//  File.swift
//  
//
//  Created by BigBoss on 10/4/20.
//

import Foundation

public struct PoolAggregateList : Codable {


    let count : Int
    let data : [PoolAggregate]

    enum CodingKeys: String, CodingKey {
            case count = "count"
            case data = "data"
    }

    public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            count = try values.decode(Int.self, forKey: .count)
            data = try values.decode([PoolAggregate].self, forKey: .data)
    }

}


public struct PoolAggregate : Codable {

    let asset : String
    let assetdepth : String
    let avgassetdepth : String
    let avgpoolunits : String
    let avgrunedepth : String
    let bucket : String
    let poolunits : String
    let runedepth : String

    enum CodingKeys: String, CodingKey {
            case asset = "asset"
            case assetdepth = "assetdepth"
            case avgassetdepth = "avgassetdepth"
            case avgpoolunits = "avgpoolunits"
            case avgrunedepth = "avgrunedepth"
            case bucket = "bucket"
            case poolunits = "poolunits"
            case runedepth = "runedepth"
    }

    public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            asset = try values.decode(String.self, forKey: .asset)
            assetdepth = try values.decode(String.self, forKey: .assetdepth)
            avgassetdepth = try values.decode(String.self, forKey: .avgassetdepth)
            avgpoolunits = try values.decode(String.self, forKey: .avgpoolunits)
            avgrunedepth = try values.decode(String.self, forKey: .avgrunedepth)
            bucket = try values.decode(String.self, forKey: .bucket)
            poolunits = try values.decode(String.self, forKey: .poolunits)
            runedepth = try values.decode(String.self, forKey: .runedepth)
    }

}
