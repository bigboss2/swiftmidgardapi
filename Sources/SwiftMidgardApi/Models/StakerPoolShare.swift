//
//  File.swift
//  
//
//  Created by BigBoss on 9/12/20.
//

import Foundation
import BigInt
import BigNumber

public struct StakerPoolShare: Identifiable {
    public var id: UUID = UUID()
    public var asset: String
    public var assetShare: BigInt
    public var runeShare: BigInt
    public var stakeUnit: BigInt
    public var percentage: Decimal
    public var assetValue: BDouble
    public var runeValue: BDouble
    public var address: String = ""
    
    public init() {
        self.asset = ""
        self.assetShare = 0
        self.runeShare = 0
        self.stakeUnit = 0
        self.percentage = 0
        self.assetValue = 0
        self.runeValue = 0
    }
    
    public init(poolDetail: PoolDetail, stakeUnit: String, address: String = "", runeUSDPrice: String?){
        
        self.asset = poolDetail.asset
        self.address = address
                    self.assetShare = 0
                    self.runeShare = 0
                    self.stakeUnit = 0
        self.percentage = 0
        if let stakeUnit = BigInt(stakeUnit),
           let poolUnit = BigInt(poolDetail.units),
           let assetDepth = BigInt(poolDetail.assetDepth),
           let runeDepth = BigInt(poolDetail.runeDepth),
           let priceUSD = BDouble(poolDetail.assetPriceUSD),
           let runePrice = BDouble(poolDetail.assetPrice)
           {
            let runeShare = (runeDepth * stakeUnit)/poolUnit
            let assetShare = (assetDepth * stakeUnit)/poolUnit

            self.assetShare = assetShare
            self.runeShare = runeShare
            self.stakeUnit = stakeUnit
            let percentage = Double(stakeUnit) / Double(poolUnit)
            self.percentage = Decimal(percentage) * 100
            self.assetValue = (BDouble(exactly: assetShare) ?? 0 ) * priceUSD
            let runePriceUSD = (1 / runePrice) * priceUSD
            self.runeValue = (BDouble(exactly: runeShare) ?? 0) * runePriceUSD
            
            print("---start price: \(poolDetail.asset)")
            print(poolDetail)
//            print()
            print(assetShare.asBaseValue())
            print(runeShare.asBaseValue())
            print((BDouble(exactly: assetShare) ?? 0).asBaseValue)
            print((BDouble(exactly: runeShare) ?? 0).asBaseValue)
            print(priceUSD.decimalDescription)
            print(runePrice.decimalDescription)
            print(((1/runePrice) * priceUSD).decimalDescription)
            print(runeUSDPrice)
            print(self.assetValue.asBaseValue)
            print(self.runeValue.asBaseValue)
            print((self.assetValue + self.runeValue).asBaseValue)
        }else{
            self.assetShare = 0
            self.runeShare = 0
            self.stakeUnit = 0
            self.percentage = 0
            self.runeValue = 0
            self.assetValue = 0
        }
    }
    
}



extension StakerPoolShare: CustomStringConvertible {
    
    public var totalValue: BDouble {
        return self.assetValue + self.runeValue
    }
    
    public var description: String {
        return "addess: \(self.address)" +
            "(asset: \(asset), share: \(assetShare.bnbChainBNvalue))\n" +
        "(asset: \(asset), share: \(runeShare.bnbChainBNvalue))\n" +
        "(stakeUnit: \(stakeUnit.bnbChainBNvalue)\n" +
        "(share: \((percentage * 100).asValue())\n" +
        "(Rune $: \(runeValue.asBaseValue)\n" +
        "(Asset $: \(assetValue.asBaseValue)\n" +
            "(Totsl $: \((assetValue + runeValue).asBaseValue)"
    }
    
    static func assetName(asset: String) -> String {
        let array = asset.split(separator: ".")
        guard let second = array[safe: 1] else {
            return asset
        }
        
        if second.contains("-"){
            return String(second.split(separator: "-").first ?? second)
        }else{
            return String(second)
        }
    }
    
    public var assetName: String {
        return StakerPoolShare.assetName(asset: self.asset)
    }
    
    public var blockchain: String {
        if self.asset.contains("BNB.") {
            return self.asset.contains("BNB.BNB") ? "Native" : "BEP2"
        }else if self.asset.contains("ETH.") {
            return self.asset.contains("ETH.ETH") ? "Native" : "ERC-20"
        }else {
            return "Native"
        }
        
        
        let array = self.asset.split(separator: ".")
        guard let second = array[safe: 1] else {
            return self.asset
        }
        
        if second.contains("-"){
            return String(second.split(separator: "-").first ?? second)
        }else{
            return String(second)
        }
    }
}

//        let poolUnit = BigInt(39679602537529)
//        let assetDepthBN = BigInt(2664114360079)
//
//        let runeDepthBN = BigInt(81198191489634)
//        let stakeUnitsBN = BigInt(160153240661)
//
////        const runeShare = poolUnitsBN
////          ? runeDepthBN.multipliedBy(stakeUnitsBN).div(poolUnitsBN)
////          : bn(0);
////        const assetShare = poolUnitsBN
////          ? assetDepthBN.multipliedBy(stakeUnitsBN).div(poolUnitsBN)
////          : bn(0);
//
//        let runeShare = (runeDepthBN * stakeUnitsBN)/poolUnit
//        let assetShare = (assetDepthBN * stakeUnitsBN)/poolUnit
//
//        print("stake unit: \(stakeUnitsBN.amoutString)")
//        print("runeShare: \(runeShare.amoutString)")
//        print("assetShare: \(assetShare.amoutString)")
        
