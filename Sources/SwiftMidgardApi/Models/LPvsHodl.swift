//
//  File.swift
//  
//
//  Created by Bigboss on 18.04.21.
//

import Foundation
import BigInt
import BigNumber

public struct LPvsHodl {

    public var value: BDouble
    public var percentage: Decimal
    public var apy: String
    public var day: Int
    
    public init(value: BDouble = 0, percent: Decimal = 0, apy: String = "", day: Int = 0) {
        self.value = value
        self.percentage = percent
        self.apy = apy
        self.day = day
    }

    public init (stakedState: StakedState, poolShare: StakerPoolShare, poolDetail: PoolDetail){
        let stakedCurrent = Self.getStakeCurrent(stakedState: stakedState, poolDetail: poolDetail)
        print("current staked \(poolShare.assetName): \(stakedCurrent.asBaseValue)")
        let now = poolShare.assetValue + poolShare.runeValue
        print("now staked \(poolShare.assetName): \(now.asBaseValue)")
        print("now staked \(poolShare.assetName): \(poolShare.totalValue.asBaseValue)")
        let profitAmount = now - stakedCurrent
        let devide = profitAmount / stakedCurrent
//        let percent = (Double(devide.quotient) + Double(devide.remainder) / Double(stakedCurrent)) * 100
        value = profitAmount
        percentage = Decimal(string: (devide * 100).decimalDescription) ?? 0
        let dayCount = Self.getDay(time: stakedState.firststake)
        apy = Self.calculateAPY(dayCount: dayCount, percentageNumber: percentage)
        day = dayCount
        print(day)
    }
    
    static func calculateAPY(dayCount: Int, percentageNumber: Decimal) -> String{
        if dayCount < 7 {
            return "shows after 7 days"
        }
        var dayLimit = 0 // dayCount >= 14 ? 14 : 7;
        var maxLimit = 0
        var base = 0
        if (dayCount >= 30) {
            dayLimit = 30
            maxLimit = 12
            base = 30
        } else if (dayCount >= 14) {
            dayLimit = 14
            maxLimit = 52
            base = 7
        } else {
            dayLimit = 7
            maxLimit = 52
            base = 7
        }
        
        let periodic = Double(dayCount) / Double(dayLimit);
                let periodicRatePercent = percentageNumber / Decimal(periodic);
        let periodicRate = periodicRatePercent / 100;
        let numberOfWeekOrMonth = maxLimit / (dayLimit / base)
        
        let apy = (pow((periodicRate + 1), numberOfWeekOrMonth) - 1 ) * 100
        return "\(apy.asCurrency())%"
//        const apy = (((periodicRate.plus(1)).pow(numberOfWeekOrMonth)).minus(1)).multipliedBy(100);
    }
    
    
    static func getDay(time: Double) -> Int {
        let date = Date(timeIntervalSince1970: time)
        print(time)
        print(date)
        
        let timeInterval = Double(Date().timeIntervalSince1970)
        let diff = timeInterval - time
        let count = ceil(diff / (3600 * 24))
        print("Date time: \(Date().days(sinceDate: date) ?? 0)")
        print("Real time: \(ceil(diff / (3600 * 24)))")
        print(timeInterval)
        
        return Int(count)
    }
    
    static func getApy(time: Double, number: Decimal) -> String{
        return ""
    }
    
//    const profitAmount = nowUSD.minus(currentOriginal);
//    // console.log(`after profit: ${profitAmount.div(this.baseNumber).toFixed(2)}`);
//    const percent = (profitAmount.div(currentOriginal)).multipliedBy(100);
    
    static func getStakeCurrent(stakedState: StakedState, poolDetail: PoolDetail) -> BDouble{
        let runeValue = (stakedState.postrunestakeBN / poolDetail.assetPriceBN) * poolDetail.assetPriceUSDBN
        print(runeValue)
        let asssetValue = poolDetail.assetPriceUSDBN * stakedState.postassetstakeBN
        return runeValue + asssetValue
    }
    
    
}



extension Date {

    func years(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.year], from: sinceDate, to: self).year
    }

    func months(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.month], from: sinceDate, to: self).month
    }

    func days(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.day], from: sinceDate, to: self).day
    }

    func hours(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.hour], from: sinceDate, to: self).hour
    }

    func minutes(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.minute], from: sinceDate, to: self).minute
    }

    func seconds(sinceDate: Date) -> Int? {
        return Calendar.current.dateComponents([.second], from: sinceDate, to: self).second
    }

}
