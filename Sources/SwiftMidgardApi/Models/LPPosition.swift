//
//  File.swift
//  
//
//  Created by Bigboss on 08.05.21.
//

import Foundation

public struct LPPositions {
    var list: [LPPosition]
    
    var pool: String {
        return list[0].pair
    }
    
    var address: String {
        return list[0].txgroup ?? ""
    }
    
    mutating func addLatest(lpPosition: LPPosition){
        self.list.append(lpPosition)
    }
}

public struct LPUnit: Codable {
    let pool : String
    let poolunits : String
    let txgroup : String
}

public struct LPPosition: Codable {
    let height : String?
    let liquidityTokenBalance : Double
    let liquidityTokenTotalSupply : Double
    let pair : String
    let reserve0 : Double
    let reserve1 : Double
    let reserveUSD : Double
    let token0PriceUSD : Double
    let token1PriceUSD : Double
    let txgroup: String?

    enum CodingKeys: String, CodingKey {
            case height = "height"
            case liquidityTokenBalance = "liquidityTokenBalance"
            case liquidityTokenTotalSupply = "liquidityTokenTotalSupply"
            case pair = "pair"
            case reserve0 = "reserve0"
            case reserve1 = "reserve1"
            case reserveUSD = "reserveUSD"
            case token0PriceUSD = "token0PriceUSD"
            case token1PriceUSD = "token1PriceUSD"
            case txgroup = "txgroup"
    }
    
    public init(height: String? = nil,
                liquidityTokenBalance : Double  = 0.0,
                 liquidityTokenTotalSupply : Double = 0.0,
                 pair : String = "",
                 reserve0 : Double = 0.0,
                 reserve1 : Double = 0.0,
                 reserveUSD : Double = 0.0,
                 token0PriceUSD : Double = 0.0,
                 token1PriceUSD : Double = 0.0,
                 txgroup: String? = nil) {
        self.height = height
        self.liquidityTokenBalance = liquidityTokenBalance
        self.liquidityTokenTotalSupply = liquidityTokenTotalSupply
        self.pair = pair
        self.reserve0 = reserve0
        self.reserve1 = reserve1
        self.token0PriceUSD = token0PriceUSD
        self.token1PriceUSD = token1PriceUSD
        self.reserveUSD = reserveUSD
        self.txgroup = txgroup
    }
    

    public init(from decoder: Decoder) throws {
            let values = try decoder.container(keyedBy: CodingKeys.self)
            height = try values.decode(String.self, forKey: .height)
            liquidityTokenBalance = try values.decode(Double.self, forKey: .liquidityTokenBalance)
            liquidityTokenTotalSupply = try values.decode(Double.self, forKey: .liquidityTokenTotalSupply)
            pair = try values.decode(String.self, forKey: .pair)
            reserve0 = try values.decode(Double.self, forKey: .reserve0)
            reserve1 = try values.decode(Double.self, forKey: .reserve1)
            reserveUSD = try values.decode(Double.self, forKey: .reserveUSD)
            token0PriceUSD = try values.decode(Double.self, forKey: .token0PriceUSD)
            token1PriceUSD = try values.decode(Double.self, forKey: .token1PriceUSD)
            txgroup = try values.decodeIfPresent(String.self, forKey: .txgroup)
    }
    
    public static func makeLPPosition(poolShare: StakerPoolShare, poolDetail: PoolDetail, height: String = "") -> LPPosition{
        let lpBalance = poolShare.stakeUnit.amount(decimal: 8, isFull: true)
        let lpSupply = poolDetail.units.toBDouble
        let reserve0 = poolDetail.runeDepth.toBDouble
        let reserve1 = poolDetail.assetDepth.toBDouble
        let token0PriceUSD = poolDetail.runePriceSUDBN
        let token1PriceUSD = poolDetail.assetPriceUSDBN
        let reserve = (reserve0 * token0PriceUSD) + (reserve1 * token1PriceUSD)
        print(lpBalance)
        print("rune$: \(token0PriceUSD.asNumber)")
        print("asset$: \(token1PriceUSD.asNumber)")
//        print(lpSupply.asBaseNumber.toDouble)
        let lpPos = LPPosition(height: height, liquidityTokenBalance: lpBalance.toDouble, liquidityTokenTotalSupply: lpSupply.asBaseNumber.toDouble,
                               pair: poolDetail.asset,
                               reserve0: reserve0.asBaseNumber.toDouble,
                               reserve1: reserve1.asBaseNumber.toDouble,
                               reserveUSD: reserve.asBaseNumber.toDouble,
                               token0PriceUSD: token0PriceUSD.asNumber.toDouble, token1PriceUSD: token1PriceUSD.asNumber.toDouble, txgroup: poolShare.address)
        return lpPos
    }

}


extension String {
    var toDouble: Double {
        return (self as NSString).doubleValue
    }
}
