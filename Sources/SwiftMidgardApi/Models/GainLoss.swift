//
//  File.swift
//  
//
//  Created by Bigboss on 19.04.21.
//

import Foundation
import BigInt
import BigNumber

public struct GainLoss{
    public var value: BDouble
    public var percentage: BDouble
    public var rune: BDouble
    public var asset: BDouble
    
    public var percentageString: String {
        return (self.percentage * 100).asValue
    }
    
    public var valueString: String {
        return self.value.asBaseValue()
    }
    
    
    public init(value: BDouble = BDouble(0), percent: BDouble = BDouble(0), rune: BDouble = BDouble(0), asset: BDouble = BDouble(0)) {
        self.value = value
        self.percentage = percent
        self.rune = rune
        self.asset = asset
    }
    
    public init (stakedState: StakedState, poolShare: StakerPoolShare){
       
        let totalStakeUSD = BDouble(stakedState.totalstakedusd)!
        let totalUnStakeUSD = BDouble(stakedState.totalunstakedusd)!
        let currentUSD = poolShare.totalValue
        
        let totalCurrentUSD = totalUnStakeUSD + currentUSD
        let gainLoss = totalCurrentUSD - totalStakeUSD
        let percentage = gainLoss / totalStakeUSD
        
        
        
        let runeAmount = poolShare.runeShare.asBDoule - stakedState.runestake.asBDoule - stakedState.runewithdrawn.asBDoule
        let assetAmount = poolShare.assetShare.asBDoule - stakedState.assetstake.asBDoule - stakedState.assetwithdrawn.asBDoule
        
        print("-------GAIN/LOSS---------")
        print("rune stake: \(stakedState.runestake.asBDoule) :runeWithdrawn: \(stakedState.runewithdrawn.asBDoule)")
        print(totalStakeUSD.asBaseValue())
        print(totalUnStakeUSD.asBaseValue())
        print(currentUSD.asBaseValue())
        print(percentage.asValue)
        print((percentage * 100).asValue)
        print(gainLoss.asBaseValue())
        print(BDouble.base.asValue)
        print("")
        print("G/L Rune: \(runeAmount.asBaseValue())")
        print("G/L Asset: \(assetAmount.asBaseValue())")
        self.value = gainLoss
        self.percentage = percentage
        self.rune = runeAmount
        self.asset = assetAmount
    }
    
}


extension GainLoss {
    static func createEmpty() -> GainLoss{
        return GainLoss(value: BDouble(0), percent: BDouble(0))
    }
}
