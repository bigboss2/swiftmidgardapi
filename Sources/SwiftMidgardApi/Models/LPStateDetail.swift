//
//  File.swift
//  
//
//  Created by Bigboss on 16.05.21.
//

import Foundation
import BigNumber

public struct LPStateDetail {
    public let address: String
    public let pool: String
    public let stakedStates: StakedState
    public let poolShare: StakerPoolShare
    public var lpVsHodl: LPvsHodl
    public var gainLoss: GainLoss
    public var runePriceUSD: BDouble
    public var assetPriceUSD: BDouble
    public var feeEarn: ReturnMetrics
    public var assetUrl: String
}

public extension LPStateDetail {
    static func createEmpty() -> LPStateDetail{
        return LPStateDetail.init(address: "", pool: "", stakedStates: StakedState.createEmpty(), poolShare: StakerPoolShare(), lpVsHodl: LPvsHodl(), gainLoss: GainLoss.createEmpty(), runePriceUSD: BDouble(0), assetPriceUSD: BDouble(0), feeEarn: ReturnMetrics(), assetUrl: "")
    }

}
