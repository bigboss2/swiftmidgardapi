//
//  ExerciseNetworking.swift
//  Exercises
//
//  Created by Dumbo on 17/04/2020.
//  Copyright © 2020 BigBoss. All rights reserved.
//

import Foundation
import Moya
import PromiseKit

open class CompanionNetwork: NetworkProtocol {
    public func getWalletBalance(server: ServerType, param: BalanceParameter) -> Promise<WalletBalance> {
        return handler.getWalletBalance(server: server, param: param)
    }
    
    public func getCoinGeckoHistroy(server: ServerType, param: CoinGeckoParameter) -> Promise<[CoinGecko]> {
        return handler.getCoinGeckoHistroy(server: server, param: param)
    }
    
    
    public func getAllPoolAddresses(server: ServerType, param: PoolGroupsParameter) -> Promise<[PoolGroup]> {
        return handler.getAllPoolAddresses(server: server, param: param)
    }
    
    public func getStakedStates(server: ServerType, param: PoolGroupsParameter) -> Promise<[StakedState]> {
        return handler.getStakedStates(server: server, param: param)
    }
    
    public func getAssetPrices(server: ServerType, param: AssetsParameter) -> Promise<[AssetDetail]> {
        return handler.getAssetPrices(server:server, param:param)
    }
    
    public func getBlockPool(server: ServerType, param: BlockParameter) -> Promise<PoolBlock> {
        return handler.getBlockPool(server: server, param: param)
    }
    
    public func getPoolAggregate(server: ServerType, param: AggregateParameter) -> Promise<PoolAggregateList> {
        return handler.getPoolAggregate(server: server, param: param)
    }
    
    public func getStakerFeeEarn(server: ServerType, param: PoolGroupsParameter, poolDetail: [PoolDetail], runeUSDPrice: String) -> Promise<[ReturnMetrics]> {
        return handler.getStakerFeeEarn(server: server, param: param, poolDetail: poolDetail, runeUSDPrice: runeUSDPrice)
    }
    
    public func getPools(server: ServerType, param: PoolParameter) -> Promise<[String]> {
        return handler.getPools(server: server, param: param)
    }
        
    public func getPoolsDetail(server: ServerType, param: PoolParameter) -> Promise<PoolDetail> {
        return handler.getPoolsDetail(server: server, param: param)
    }
    
    public func getStakerPoolData(server: ServerType, param: StakerPoolDataParameter) -> Promise<[StakersAssetData]> {
        return handler.getStakerPoolData(server: server, param: param)
    }
    
    public func getStakerDetail(server: ServerType, param: StakerDetailParameter) -> Promise<StakersAddressData> {
        return handler.getStakerDetail(server: server, param: param)
    }
    
    public func getTxs(server: ServerType, param: Txs) -> Promise<Transaction> {
        return handler.getTxs(server: server, param: param)
    }
    
    public func getStakerSummary(midgardServer: ServerType, asgardServer: ServerType, address: [String], runePriceUSD: String) -> Promise<StakerSummary> {
        return handler.getStakerSummary(midgardServer: midgardServer, asgardServer: asgardServer, address: address, runePriceUSD: runePriceUSD)
    }
    
    public func getStakedState(server: ServerType, param: StakedStateParameter) -> Promise<StakedState> {
        return handler.getStakedState(server: server, param: param)
    }
    
    public func getPoolImages(server: ServerType, param: PoolImageParameter) -> Promise<[PoolImage]> {
        return handler.getPoolImages(server: server, param: param)
    }
    
    public func getRunePrice(midgardServer: ServerType, param: GlobalStateParam) -> Promise<GlobalState> {
        return handler.getRunePrice(midgardServer: midgardServer, param: param)
    }

    let handler: NetworkHandler
    
    init(networkHandler: NetworkHandler) {
        self.handler = networkHandler
    }
}


