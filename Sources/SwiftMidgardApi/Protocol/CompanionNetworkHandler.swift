//
//  CompanionNetworkHandler.swift
//  Networking
//
//  Created by BigBoss on 7/29/20.
//  Copyright © 2020 BigBoss. All rights reserved.
//

import Foundation
import Moya
import PromiseKit


open class NetworkHandler: NetworkProtocol {
    public func getWalletBalance(server: ServerType, param: BalanceParameter) -> Promise<WalletBalance> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: WalletBalance.self) { data in
            return WalletBalance.decodeBalace(data: data, chain: param.chain)
        }
    }
    
    public func getCoinGeckoHistroy(server: ServerType, param: CoinGeckoParameter) -> Promise<[CoinGecko]> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [CoinGecko].self) { data in
            return CoinGecko.decode(data: data)
        }
    }
    
    public func getBlockPool(server: ServerType, param: BlockParameter) -> Promise<PoolBlock> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: PoolBlock.self)
    }
    
    public func getPoolAggregate(server: ServerType, param: AggregateParameter) -> Promise<PoolAggregateList> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: PoolAggregateList.self)
    }
    
    
//    public func getStakerAllPoolsShare(server: ServerType, address: String) -> Promise<[StakerPoolShare]> {
//        return firstly {
//            self.getStakerDetail(server: server, param: StakerDetailParameter.init(address: address))
//        }.then { (creds: StakersAddressData) in
//            self.getStakerPoolsShare(server: server, address: address, asset: creds.poolsArray ?? [])
//        }
//    }
    
    public func getStakerSummary(midgardServer: ServerType, asgardServer: ServerType, address: [String], runePriceUSD: String) -> Promise<StakerSummary> {
        return firstly {
            self.getStakedStates(server: asgardServer, param: PoolGroupsParameter.init(addresses: address, path: .stakedList))
        }.then { (creds: [StakedState]) in
            self.getStakerSummaryDetail(midgardServer: midgardServer, asgardServer: asgardServer, pool: creds.map{$0.pool}, stakedStates: creds, runePriceUSD: runePriceUSD)
        }
    }
    
    public func getRunePrice(midgardServer: ServerType, param: GlobalStateParam) -> Promise<GlobalState> {
        return self.request(task: BaseApi.midgard(server: midgardServer, api: .get(param: param)), value: GlobalState.self)
    }
    
    public func getStakedState(server: ServerType, param: StakedStateParameter) -> Promise<StakedState> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: StakedState.self)
    }
    
//    public func getStakedStates(server: ServerType, address: String, pools: [String]) -> Promise<[StakedState]> {
//        return Promise { seal in
//            firstly{
//                when(fulfilled: pools.map{self.getStakedState(server: server, param: StakedStateParameter.init(address: address, pool: $0))})
//            }.done { stakedStates in
//                seal.fulfill(stakedStates)
//            }.catch { error in
//                seal.reject(error)
//            }
//        }
//    }
    
    public func getStakerFeeEarn(server: ServerType, param: PoolGroupsParameter, poolDetail: [PoolDetail], runeUSDPrice: String) -> Promise<[ReturnMetrics]> {
        return firstly {
            self.getAllPoolAddresses(server: server, param: param)
        }.then { (creds: [PoolGroup]) in
            self.getFeeEarn(serve: server, poolGroup: creds, poolDetail: poolDetail, runePriceUSD: runeUSDPrice)
        }
    }
    
    public func getEachPositionList(server: ServerType, param: FeeEarnParameter) -> Promise<[LPPosition]> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [LPPosition].self)
    }
    
    public func getLPUnits(server: ServerType, param: PoolGroupsParameter) -> Promise<[LPUnit]> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [LPUnit].self)
    }
    
    public func getFeeEarn(serve: ServerType, poolGroup: [PoolGroup], poolDetail: [PoolDetail], runePriceUSD: String) -> Promise<[ReturnMetrics]>{
        return Promise { seal in
            
            let positions = poolGroup.map{
                self.getEachPositionList(server: serve, param: FeeEarnParameter(group: $0.txgroup, pool: $0.pool))
            }
            let lastestUnit = self.getLPUnits(server: serve, param: PoolGroupsParameter(addresses: poolGroup.map{$0.txgroup}, path: .stakeUnit))
            
            let poolDetial = Set.init(poolGroup.map{$0.pool}).map{
                self.getPoolsDetail(server: serve, param: PoolParameter(pool: $0))
            }
            
            var tasks = [lastestUnit.asVoid()] + positions.map{$0.asVoid()}
            if poolDetail.count <= 0 {
                tasks = tasks + poolDetial.map{$0.asVoid()}
            }
            
            when(fulfilled:
                    tasks )
                .done {
                print("done")
                    guard let lpUnits = lastestUnit.value?.compactMap({$0}) else {
                        seal.fulfill([])
                        return
                    }
                    
                    let positions = positions.compactMap({$0.value}).map{LPPositions(list: $0)}
                    let pools = poolDetail.count > 0 ? poolDetail : poolDetial.compactMap({$0.value})
                    
                    let poolMap = pools.toDictionary{$0.asset}
                    print(positions)
                    
                    let poolShare = lpUnits.map{ unit -> StakerPoolShare in
                        guard let poolDetail = poolMap[unit.pool] else{
                            fatalError("No detail")
                        }
                        return StakerPoolShare(poolDetail: poolDetail, stakeUnit: unit.poolunits, address: unit.txgroup, runeUSDPrice: runePriceUSD)
                    }
                    
                    let lastPosition = poolShare.map{LPPosition.makeLPPosition(poolShare: $0, poolDetail: poolMap[$0.asset]!, height: "latest")}
                    print(lastPosition)
                    
                    let lastestPos = positions.map{ position -> LPPositions in
                        guard let last = lastPosition.filter({$0.pair == position.pool}).first else {
                            fatalError("error get lastet")
                        }
                        var tem = position
                        print("before: \(tem)")
                        tem.addLatest(lpPosition: last)
                        print("after: \(tem)")
                        return tem
                    }
                    
                    print(lastestPos)
                    let posMap = lastestPos.toDictionaryArray{$0.pool}
                    let metrics = posMap.map{ReturnMetrics.combineReturnMetric(position: $0.value)}
                    print(metrics)
                    seal.fulfill(metrics)
            }.catch { error in
                seal.reject(error)
            }
        }
        
    }
    
    
    public func getStakerSummaryDetail(midgardServer: ServerType,
                                       asgardServer: ServerType,
                                       pool: [String],
                                       stakedStates: [StakedState],
                                       runePriceUSD: String) -> Promise<StakerSummary> {
        print(pool)
        return Promise { seal in
            if pool.count <= 0 {
                seal.reject(NetworkError.other)
                return
            }
            print(stakedStates)
            print(pool)
            print(runePriceUSD)

            firstly {
                when(fulfilled:
                        pool.map{
                            self.getPoolsDetail(server: asgardServer, param: PoolParameter.init(pool: $0))
                        }
                )
                
            }.done { poolDetails in
//                print(stakeUnits)
                print(poolDetails)
//                print(assetPrices)
//                print(stakedStates)
//                var poolMap = pools.dictionary(withKey: \.asset)
//                let sharedData = stakerUnits.compactMap {StakerPoolShare.init(poolDetail: poolMap[$0.asset] , stakerData: $0)
//                }
                let result = StakerSummary.init(address: "", pools: pool,stakedStates: stakedStates, poolDetails: poolDetails, runePriceUSD: runePriceUSD)

                seal.fulfill(result)
            }.catch { error in
                seal.reject(error)
            }

        }
    }
    
    
    public func getAssetPrices(server: ServerType, param: AssetsParameter) -> Promise<[AssetDetail]> {
        self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [AssetDetail].self)
    }
    
    
//    public func getStakerPoolsShare(server: ServerType, address: String, asset: [String]) -> Promise<[StakerPoolShare]> {
//        return Promise { seal in
//            if asset.count <= 0 {
//                seal.fulfill([])
//            }
//
//            firstly {
//                when(fulfilled:
//                        self.getPoolsDetail(server: server, param: PoolDetailParameter.init(asset: asset    )),
//                     self.getStakerPoolData(server: server, param: StakerPoolDataParameter.init(address: address, assets: asset)))
//            }.done { pools, stakerUnits in
//                print(pools)
//                print(stakerUnits)
//                var poolMap = pools.dictionary(withKey: \.asset)
//                let sharedData = stakerUnits.compactMap {StakerPoolShare.init(poolDetail: poolMap[$0.asset] , stakerData: $0)
//                }
//
//                seal.fulfill(sharedData)
//            }.catch { error in
//                seal.reject(error)
//            }
//        }
//    }
    
    public func getPools(server: ServerType, param: PoolParameter) -> Promise<[String]> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [String].self)
    }
    
    public func getPoolsDetail(server: ServerType, param: PoolParameter) -> Promise<PoolDetail>{
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: PoolDetail.self)
    }
    
    public func getStakerPoolData(server: ServerType, param: StakerPoolDataParameter) -> Promise<[StakersAssetData]> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [StakersAssetData].self)
    }
    
    public func getStakerDetail(server: ServerType, param: StakerDetailParameter) -> Promise<StakersAddressData> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: StakersAddressData.self)
    }
    
    public func getTxs(server: ServerType, param: Txs) -> Promise<Transaction> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: Transaction.self)
    }
    
    
    
    
    
    let provider: MoyaProvider<BaseApi>
    
    init( provider: MoyaProvider<BaseApi>) {
        self.provider = provider
    }

//    public func pushResult(result: [String : Any]) -> Promise<DataModel.Response<Bool>> {
//        return self.request(task: CompanionApi.result(param: result), value: Bool.self)
//    }
//    
//    public func getStudyDetail(studyKey: String) -> Promise<DataModel.Response<Study>> {
//        
//        return self.getStudyDetailLocal(studyKey: studyKey)
//    }
//    
//    public func getStudyDetailLocal(studyKey: String) -> Promise<DataModel.Response<Study>> {
//        
//        return self.request(task: CompanionApi.studyDetail(studyKey: studyKey), value: Study.self)
//    }
//    
//    public func getStudySurvey(studyKey: String, surveyKey: String) -> Promise<DataModel.Response<SurveyDetail>> {
//        return self.getStudySurveyLocal(studyKey: studyKey, surveyKey: surveyKey)
//    }
//    
//    func getStudySurveyLocal(studyKey: String, surveyKey: String) -> Promise<DataModel.Response<SurveyDetail>> {
//        return self.request(task: CompanionApi.studySurvey(studyKey: studyKey, surveyKey: surveyKey), value: SurveyDetail.self)
//    }
//    
//    public func getBaseLineStudy() -> Promise<DataModel.Response<Study>> {
//        return self.getBaseLineStudyLocal()
//    }
//    
//    func getBaseLineStudyLocal() -> Promise<DataModel.Response<Study>> {
//       fatalError()
//    }
//
//    public func getStudyImageUrl(studyKey: String) -> String {
//        let task = CompanionApi.studyImage(studyKey: studyKey)
//        return task.fullPathDescription
//    }
//    
//    public func getStudyList() -> Promise<DataModel.Response<StudyList>> {
//        return self.request(task: CompanionApi.studyList, value: StudyList.self)
//    }
//    
    func request<T: Codable>(task: BaseApi, value: T.Type, decoder: ((Data) -> Swift.Result<T, NetworkingError>)? = nil) -> Promise<T> {
        print(task.fullPathDescription)
        return Promise { seal in
            provider.request(task) { result in
                switch result {
                case .success(let response):
                    do {
                        if response.statusCode == 200 || response.statusCode == 400 {
                           
                            if let decoder = decoder{
                                do {
                                    let decoded = try decoder(response.data).get()
                                    seal.fulfill(decoded)
                                }catch{
                                    seal.reject(error)
                                }
                            }else{
                                print("response code: \(response.statusCode)")
                                let jsonResponse = try JSONSerialization.jsonObject(with: response.data, options: [])
                                print(jsonResponse) //Response result
                                let result = try response.map(value)
                                seal.fulfill(result)
                            }
                        }
                        else if (response.statusCode == 500) {
                            print("600 No data at the height")
                            seal.reject(NetworkingError.apiFailed(code: response.statusCode, message: "600 No data at the height"))
                        }
                        else{
                            let message = try response.map(ErrorResponse.self)
                            seal.reject(NetworkingError.apiFailed(code: response.statusCode, message: message.message))
                        }
                        
                    } catch {
                        seal.reject(error)
                    }
                case .failure(let error):
                    seal.reject(error)
                    
                }
            }
        }
    }
    
}


extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
    
    public func toDictionaryArray<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:[Element]] {
        var dict = [Key:[Element]]()
        for element in self {
            if let existing = dict[selectKey(element)] {
                dict[selectKey(element)] = existing + [element]
            } else {
                dict[selectKey(element)] = [element]
            }
        }
        return dict
    }
}
