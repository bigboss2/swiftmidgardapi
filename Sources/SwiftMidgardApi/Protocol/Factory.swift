//
//  Factory.swift
//  Networking
//
//  Created by BigBoss on 6/18/20.
//  Copyright © 2020 BigBoss. All rights reserved.
//

import Foundation
import Moya
import Alamofire

public struct NetworkingFactory {
    public static func createDefault() -> CompanionNetwork{

//        let serverTrustManager = ServerTrustManager(evaluators: ["": DisabledTrustEvaluator()])
//
//        let session = Session(
//            configuration: URLSessionConfiguration.default,
//            startRequestsImmediately: false,
//            serverTrustManager: serverTrustManager
//        )

        let provider = MoyaProvider<BaseApi>()
        
        
        let handler = MockCompanionNetwork(provider: provider)
        let network = CompanionNetwork(networkHandler: handler)
        return network
    }
    
    public static func createMock() -> CompanionNetwork{
        let handler = MockCompanionNetwork(provider: MoyaProvider<BaseApi>())
        let network = CompanionNetwork(networkHandler: handler)
        return network
    }
    
//    public static func createDefault() -> NetworkProtocol{
//        let network = CompanionNetwork(provider: MoyaProvider<CompanionApi>())
//        return network
//    }
//
//    public static func createMock() -> NetworkProtocol{
//        let network = MockCompanionNetwork(provider: MoyaProvider<CompanionApi>())
//        return network
//    }
    
}
