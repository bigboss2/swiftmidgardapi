//
//  LabResNetworkProtocol.swift
//  labres
//
//  Created by BigBoss on 26/05/2020.
//  Copyright © 2020 BigBoss. All rights reserved.
//

import Foundation
import PromiseKit
//import DataModel

public enum NetworkError: Error {
    case failed409
    case other
}

public protocol NetworkProtocol {
    
    //Multichain
    func getPoolImages(server: ServerType, param: PoolImageParameter) -> Promise<[PoolImage]>
    func getAllPoolAddresses(server: ServerType, param: PoolGroupsParameter) -> Promise<[PoolGroup]>
    func getStakedStates(server: ServerType, param: PoolGroupsParameter) -> Promise<[StakedState]>
    func getStakerSummary(midgardServer: ServerType, asgardServer: ServerType, address: [String], runePriceUSD: String) -> Promise<StakerSummary>
    func getStakerFeeEarn(server: ServerType, param: PoolGroupsParameter, poolDetail: [PoolDetail], runeUSDPrice: String) -> Promise<[ReturnMetrics]>
    func getRunePrice(midgardServer: ServerType, param: GlobalStateParam) -> Promise<GlobalState>

    func getTxs(server: ServerType, param: Txs) -> Promise<Transaction>
    func getStakerDetail(server: ServerType, param: StakerDetailParameter) -> Promise<StakersAddressData>
    func getStakerPoolData(server: ServerType, param: StakerPoolDataParameter) -> Promise<[StakersAssetData]>
    func getAssetPrices(server: ServerType, param: AssetsParameter) -> Promise<[AssetDetail]>
    
    
    //MARK Pool Data
//    func getPools(server: ServerType, param: PoolParameter) -> Promise<[String]>
    func getPoolsDetail(server: ServerType, param: PoolParameter) -> Promise<PoolDetail>
    
    // Aggregate
//    func getStakerPoolsShare(server: ServerType, address: String, asset: [String] ) -> Promise<[StakerPoolShare]>
//    func getStakerAllPoolsShare(server: ServerType, address: String)->Promise<[StakerPoolShare]>

    // Asgard
    func getPoolAggregate(server: ServerType, param: AggregateParameter) -> Promise<PoolAggregateList>
    func getBlockPool(server: ServerType, param: BlockParameter) -> Promise<PoolBlock>
    
    
//    func getStakedStates(server: ServerType, address: String, pools: [String]) -> Promise<[StakedState]>
    func getStakedState(server: ServerType, param: StakedStateParameter) -> Promise<StakedState>
    
//    func
    func getWalletBalance(server: ServerType, param: BalanceParameter) -> Promise<WalletBalance>
    
    func getCoinGeckoHistroy(server: ServerType, param: CoinGeckoParameter) -> Promise<[CoinGecko]>
    
//    https://thornode.thorchain.info/bank/balances/thor1aesu2232mrtv8ujw4tkenjnnntykfsn6hgxqt8
    
//    func getStudyDetail(studyKey: String) -> Promise<Response<Study>>
//    func getStudySurvey(studyKey: String, surveyKey: String) -> Promise<Response<SurveyDetail>>
//    func getStudyImageUrl(studyKey: String) -> String
//    func pushResult(result: [String: Any]) -> Promise<Response<Bool>>
//    func getBaseLineStudy()-> Promise<Response<Study>>

}
