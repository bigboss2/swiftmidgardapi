//
//  File.swift
//  
//
//  Created by Bigboss on 18.04.21.
//

import Foundation
import Moya
import PromiseKit

public extension NetworkHandler{
    func getAllPoolAddresses(server: ServerType, param: PoolGroupsParameter) -> Promise<[PoolGroup]> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [PoolGroup].self)
    }
    
    public func getStakedStates(server: ServerType, param: PoolGroupsParameter) -> Promise<[StakedState]> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [StakedState].self)
    }
    
    func getPoolImages(server: ServerType, param: PoolImageParameter) -> Promise<[PoolImage]> {
        return self.request(task: BaseApi.midgard(server: server, api: .get(param: param)), value: [PoolImage].self)
    }
    

}
