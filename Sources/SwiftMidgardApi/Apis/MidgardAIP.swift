//
//  File.swift
//  
//
//  Created by BigBoss on 9/12/20.
//

import Foundation
import Moya


enum MidgardAPI {
    //Get details of a tx by address, asset or tx-id
    case get(param: MidgardParameter)
//    case stake(param: MidgardParameter)
    //Get Staker Data
//    Returns an array containing all the pools the staker is staking in.
//    case stakers(param: MidgardParameter)
}

extension MidgardAPI: TargetType{
    public var baseURL: URL {
        return URL.init(string: "")!
    }
    public var path: String {
        switch self {
//        case .studyList: return "/studies/api/study"
        case let .get (param):
            return param.path
        }
    }
    public var method: Moya.Method {
        switch self {
        case .get:
            return .get
        }
    }
    public var task: Task {
        switch self {
        case .get(let param):
            if let param = param.params {
                return .requestParameters(parameters: param, encoding: URLEncoding.queryString)
            }else{
                return .requestPlain
            }        }
    }
    public var validationType: ValidationType {
        switch self {
        default:
            return .none
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    public var headers: [String: String]? {
        return nil
    }
    
    public var fullPathDescription: String {
        return self.baseURL.absoluteString + self.path
    }
}
