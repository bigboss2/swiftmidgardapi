//
//  ExerciseApi.swift
//  Exercises
//
//  Created by Dumbo on 17/04/2020.
//  Copyright © 2020 BigBoss. All rights reserved.
//

import Foundation

//https://github.com/Moya/Moya/blob/master/Examples/_shared/GitHubAPI.swift

import Foundation
import Moya


// MARK: - Provider support

private extension String {
    var urlEscaped: String {
        return self.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
}

public enum ServerType {
    case baseUrl(urlString: String)
    
    var baseUrl:URL {
        switch self {
        case let .baseUrl( urlString):
            return URL.init(string: urlString)!
        }
    }
    
    public static func createServer(urlString: String = "https://midgard.thorchain.info") -> ServerType {
        return ServerType.baseUrl(urlString: urlString)
    }
    
    public static func createAsgardServer(urlString: String = "https://multichain-asgard-consumer-api.vercel.app") -> ServerType {
        return ServerType.baseUrl(urlString: urlString)
    }
    
    public static func createBalanceServer(chain: Chain) -> ServerType {
        var url = ""
        switch chain {
        case .thorchain:
            url = "https://thornode.thorchain.info"
        case .binance:
            url = "https://explorer.binance.org"
        case .litecoin:
            url = "https://sochain.com"
        case .bitcoin, .bitcoincash:
            url = "https://api.haskoin.com"
        default:
            url = "https://api.ethplorer.io"
        }
        return ServerType.baseUrl(urlString: url)
    }
    
    public static func createBlockServer(urlString: String = "http://localhost:3200") -> ServerType {
        return ServerType.baseUrl(urlString: urlString)
    }
    
    public static func createCoinGeckoServer(urlString: String = "https://api.coingecko.com") -> ServerType {
        return ServerType.baseUrl(urlString: urlString)
    }
    
//    http://localhost:3001/api/testchange?pool=BNB.BNB&height=200010
    
//    http://localhost:3200/?pool=BNB.BUSD-BD1&height=698316
}

enum BaseApi {
    case midgard(server: ServerType, api: MidgardAPI)
//    case asgard(server: ServerType, api: MidgardAPI)
}
//api/study/{studyKey}
extension BaseApi: TargetType {
    public var baseURL: URL {
        switch self {
        case let .midgard(server,_):
            return server.baseUrl
        }
    }
    public var path: String {
        switch self {
//        case .studyList: return "/studies/api/study"
//        case let .studyDetail(studyKey): return "/studies/api/study/" + studyKey
//        case let .studySurvey(studyKey, surveyKey): return "/studies/api/study/" + studyKey + "/survey/" + surveyKey
//        case let .studyImage(studyKey): return "/studies/api/study/" + studyKey + "/image"
//        case .result: return "/results/fhir"
        case .midgard(_, let api):
            return api.path
        }
    }
    public var method: Moya.Method {
        switch self {
        case .midgard(_, let api):
            return api.method
        }
    }
    public var task: Task {
        switch self {
        case .midgard(_, let api):
            return api.task
        }
    }
    public var validationType: ValidationType {
        switch self {
        default:
            return .none
        }
    }
    
    public var sampleData: Data {
        return Data()
    }
    public var headers: [String: String]? {
        return nil
//        switch self {
//        case let .activateCode(_, _, _, _, header):
//            return ["Authorization": "Bearer "+header]
//        case let .viewResult(_, _, header):
//            return ["Authorization": "Bearer "+header]
//        }
    }
    
    public var fullPathDescription: String {
        return self.baseURL.absoluteString + self.path
    }
}

public func url(_ route: TargetType) -> String {
    return route.baseURL.appendingPathComponent(route.path).absoluteString
}

// MARK: - Response Handlers

extension Moya.Response {
    func mapNSArray() throws -> NSArray {
        let any = try self.mapJSON()
        guard let array = any as? NSArray else {
            throw MoyaError.jsonMapping(self)
        }
        return array
    }
}
