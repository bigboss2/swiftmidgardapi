//
//  File.swift
//  
//
//  Created by BigBoss on 9/12/20.
//

import Foundation
import BigInt

public extension BigInt {
    
    
    public var bnbChainBNvalue: String {
        let numberFormatter = EtherNumberFormatter.short
        return numberFormatter.string(from: self, decimals: 8)
    }
    
    
    public var readable: String {
        let numberFormatter = EtherNumberFormatter.short
        return numberFormatter.string(from: self, decimals: 1)
    }
    
    
    public func bnbConvert(with number: Double) -> String {
        var bn =   Double(bnbChainBNvalue.replacingOccurrences(of: ",", with: "")) ?? 0
        let real = bn
//        bn = bn
 //       return "\(real * number)"
        let formatter = NumberFormatter()
        formatter.locale = Locale.current // Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        return formatter.string(from: (real * number) as NSNumber) ?? ""
    }
}
