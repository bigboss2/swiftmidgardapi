//
//  File.swift
//  
//
//  Created by Bigboss on 29.08.21.
//

import Foundation

public enum Chain: String, Codable {
    case binance
    case thorchain
    case ethereum
    case bitcoin
    case bitcoincash
    case litecoin
    case unknown
    case all
    
    var symbol: String {
        switch self {
        case .bitcoin: return "btc"
        case .bitcoincash: return "bch"
        default:
            return self.rawValue
        }
    }
    
    public var name: String{
        switch self {
        case .bitcoin: return "Bitcoin"
        case .bitcoincash: return "Bitcoin Cash"
        case .all: return "All Chains"
        default:
            return self.rawValue.capitalized
        }
    }
    
    var url: String {
        switch self {
        case .binance:
            return "https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/binance/info/logo.png"
        case .thorchain: return "https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/thorchain/info/logo.png"
        case .ethereum: return "https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/ethereum/info/logo.png"
        case .bitcoin: return "https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/bitcoin/info/logo.png"
        case .bitcoincash: return "https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/bitcoincash/info/logo.png"
        case .litecoin: return "https://raw.githubusercontent.com/trustwallet/assets/master/blockchains/litecoin/info/logo.png"
        default: return ""
        }
    }
    
    var symbolId: (id: String, symbol: String) {
        switch self {
        case .thorchain: return ("thorchain","RUNE")
        case .ethereum: return ("ethereum","ETH")
        case .litecoin: return ("litecoin","LTC")
        case .bitcoin: return ("bitcoin", "BTC")
        case .bitcoincash: return ("bitcoin-cash", "BCH")
        default:
            return ("","")
        }
    }
}

