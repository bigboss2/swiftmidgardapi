//
//  NetworkingError.swift
//  Networking
//
//  Created by BigBoss on 6/19/20.
//  Copyright © 2020 BigBoss. All rights reserved.
//

import Foundation

public enum NetworkingError: Error {
    case failed
    case apiFailed(code: Int, message: String)
}
