//
//  CollectionExt.swift
//  ExplorerSwift
//
//  Created by BigBoss on 9/6/20.
//

import Foundation

extension Collection {

    /// Returns the element at the specified index if it is within bounds, otherwise nil.
    subscript (safe index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
    
    func dictionary<Key>(withKey key: KeyPath<Element, Key>) -> [Key: Element] {
            return reduce(into: [:]) { dictionary, element in
                let key = element[keyPath: key]
              //  let value = element[keyPath: value]
                dictionary[key] = element
            }
        }
    
    
}

extension Date {
    
    var millisecondsSince1970:Int64 {
           return Int64((self.timeIntervalSince1970 * 1000.0).rounded())
           //RESOLVED CRASH HERE
       }

       init(milliseconds:Int64) {
         //  let date = Date(timeIntervalSince1970: self)
           self = Date(timeIntervalSince1970: TimeInterval(milliseconds))
       }
       
       func toString(format: String? = "MM-dd-yyyy HH:mm")-> String {
           let formatter = DateFormatter()
           formatter.dateFormat = format
           return formatter.string(from: self)
           
       }
    
    func timeAgoSinceNow(numericDates: Bool = true) -> String {
        let calendar = Calendar.current
        let now = Date()
        let earliest = (now as NSDate).earlierDate(self)
        let latest = (earliest == now) ? self : now
        let components: DateComponents = (calendar as NSCalendar).components([NSCalendar.Unit.minute,
                                                                              NSCalendar.Unit.hour,
                                                                              NSCalendar.Unit.day,
                                                                              NSCalendar.Unit.weekOfYear,
                                                                              NSCalendar.Unit.month,
                                                                              NSCalendar.Unit.year,
                                                                              NSCalendar.Unit.second],
                                                                             from: earliest,
                                                                             to: latest,
                                                                             options: NSCalendar.Options())

        guard
            let year = components.year,
            let month = components.month,
            let weekOfYear = components.weekOfYear,
            let day = components.day,
            let hour = components.hour,
            let minute = components.minute,
            let second = components.second
        else { return "A while ago"}

        if year >= 1 {
            return year >= 2 ? "\(year) years ago" : numericDates ? "1 year ago" : "Last year"
        } else if month >= 1 {
            return month >= 2 ? "\(month) months ago" : numericDates ? "1 month ago" : "Last month"
        } else if weekOfYear >= 1 {
            return weekOfYear >= 2 ? "\(weekOfYear) weeks ago" : numericDates ? "1 week ago" : "Last week"
        } else if day >= 1 {
            return day >= 2 ? "\(day) days ago" : numericDates ? "1 day ago" : "Yesterday"
        } else if hour >= 1 {
            return hour >= 2 ? "\(hour) hours ago" : numericDates ? "1 hour ago" : "An hour ago"
        } else if minute >= 1 {
            return minute >= 2 ? "\(minute) minutes ago" : numericDates ? "1 minute ago" : "A minute ago"
        } else {
            return second >= 3 ? "\(second) seconds ago" : "Just now"
        }
    }
    
    
}
