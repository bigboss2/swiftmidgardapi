//
//  File.swift
//  
//
//  Created by Bigboss on 18.04.21.
//

import Foundation
import BigNumber
import BigInt


extension Decimal {
    
    /// Truncate to N decimals
    /// - Parameter numberOfDecimals: Number of decimals to truncate to.
    /// - Returns: Truncated Decimal number. e.g. 4.123 truncated to 2 returns 4.12
    public func truncate(_ numberOfDecimals : Int) -> Decimal {
        var roundedValue : Decimal = 0
        var mutableSelf = self
        NSDecimalRound(&roundedValue, &mutableSelf, numberOfDecimals, .plain)
        return roundedValue
    }
    
    public func format(decimal: Int) -> String {
        return self.formatter(decimal: decimal).string(from: self as NSNumber)!
    }
    
    public func asCurrency(asset: String? = nil) -> String {
        return self.format(decimal: asset != nil ? BigInt.getAssetMaxDigit(asset: asset!) : 2)
    }
    
    public func asValue() -> String {
        return ""
    }
    
    private func formatter(decimal: Int) -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = decimal
        formatter.maximumFractionDigits = decimal
        return formatter
    }
}


public extension String {
    func addSign(sign: String = "$") -> String {
        var prefixed: Bool = false
        if self.contains("+") || self.contains("-") {
            prefixed = true
        }
        if prefixed {
            let prefix = self.prefix(1)
            let value = self.replacingOccurrences(of: "-", with: "").replacingOccurrences(of: "+", with: "")
            return "\(prefix)\(sign)\(value)"
        }else {
            return "\(sign)\(self)"
        }
    }
    
    var asPositive: String {
        if self.contains("-") {
            return self
        }
        return "+\(self)"
    }
}

public extension BigInt {
    
    func asBaseValue(asset: String = "USD") -> String {
        let numberFormatter = EtherNumberFormatter.assetFormatter(maxDigits: Self.getAssetMaxDigit(asset: asset))
        return numberFormatter.string(from: self, decimals: 8)
    }
    
    func asValue(asset: String = "USD") -> String {
        let numberFormatter = EtherNumberFormatter.assetFormatter(maxDigits: Self.getAssetMaxDigit(asset: asset))
        return numberFormatter.string(from: self, decimals: 8)
    }
    
    static func getAssetMaxDigit(asset: String) -> Int {
        if asset.contains("BTC") || asset.contains("BCH") || asset.contains("ETH") {
            return 5
        }else if asset.contains("USD"){
            return 2
        }else{
            return 3
        }
    }
}

public extension Double {
    func asValue(asset: String) -> String {
        return String(format: "%.\(BigInt.getAssetMaxDigit(asset: asset))f", self)
    }
}


public extension BDouble {
    static var base: BDouble {
        return BDouble(100000000)
    }
    
    var asValue: String {
        let string = self.decimalDescription
        return Decimal(string: string)?.asCurrency() ?? "0"
    }
    
//    var asBaseValue: String {
//        let string = (self / Self.base).decimalDescription
//        return Decimal(string: string)?.asCurrency() ?? "0"
//    }
    
    func asUSDValue(decimal: Double) -> String {
        let string = (self / BDouble(pow(10, decimal))).decimalDescription
        return Decimal(string: string)?.asCurrency(asset:  "USD") ?? "0"
    }
    
    func asBaseValue(asset: String = "USD") -> String{
        let string = (self / Self.base).decimalDescription
        return Decimal(string: string)?.asCurrency(asset: asset) ?? "0"
    }
    
    var asBaseNumber: String {
        return (self / Self.base).decimalExpansion(precisionAfterDecimalPoint: 8)
    }
    
    func asBaseNumber(decimalPoint: Int) -> String {
        return (self / Self.base).decimalExpansion(precisionAfterDecimalPoint: decimalPoint)
    }
    
    var asNumber: String {
        return self.decimalExpansion(precisionAfterDecimalPoint: 8)
    }
}
