//
//  PromiseLoader.swift
//  Exercises
//
//  Created by BigBoss on 17/04/2020.
//  Copyright © 2020 BigBoss. All rights reserved.
//

import Foundation
import PromiseKit
//import DataModel

public struct PromiseLoader<T> {
    public static func load(tasks: [Promise<T>],callbackQueue: DispatchQueue = .main, handler: @escaping ((Swift.Result<[T], Error> )-> Void) ) {
//        when(fulfilled: tasks).done { result in
//            handler(.success(result))
//        }.catch { error in
//            handler(.failure(error))
//        }
        firstly {
            when(fulfilled: tasks)
        }.done(on: callbackQueue) { results in
            handler(.success(results))
        }.catch(on: callbackQueue) { error in
            handler(.failure(error))
        }
    }
    
    public static func load(task: Promise<T>,callbackQueue: DispatchQueue = .main, handler: @escaping ((Swift.Result<T, Error> )-> Void) ) {
//        print("going to seelp")
////        do {
////            sleep(1)
////        }
//        print("wake from sleep")
        firstly {
            task
        }.done(on: callbackQueue) { result in
            handler(.success(result))
        }.catch(on: callbackQueue) {error in
            print(error.localizedDescription)
            handler(.failure(error))
        }
        
    }
    
    public static func chain<U>(task: Promise<T>, with: @escaping((T) -> Promise<U>),callbackQueue: DispatchQueue = .main, handler: @escaping ((Swift.Result<U, Error> )-> Void) ){
        firstly {
            task
        }.then { result in
            return with(result)
        }.done(on: callbackQueue) { result  in
            handler(.success(result))
        }.catch(on: callbackQueue) {error in
            print(error.localizedDescription)
            handler(.failure(error))
        }
    }

}
