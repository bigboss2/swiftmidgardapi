//
//  File.swift
//  
//
//  Created by Bigboss on 03.10.21.
//

import Foundation

public struct CoinGeckoParameter: MidgardParameter {
    public var spikeline: Bool
    public var ids: String
    public var currency: String
    
    public init(spikeline: Bool, ids: String, currency: String = "USD") {
        self.spikeline = spikeline
        self.ids = ids
        self.currency = currency
    }
    
    public var path: String {
        return "/api/v3/coins/markets"
    }
    
    public var params: [String: Any]? {
        var param: [String: Any] = [:]
        param["vs_currency"] = self.currency
        param["ids"] = ids
        param["sparkline"] = "\(spikeline)"
        return param
    }
}

