//
//  File.swift
//  
//
//  Created by Bigboss on 08.05.21.
//

import Foundation

public struct FeeEarnParameter: MidgardParameter {
    public var group: String
    private var pool: String
    
    public init(group: String, pool: String){
        self.pool = pool
        self.group = group
    }
    
    public var path: String{
        return "/api/v3/member/fee"
    }
    
    public var params: [String : Any]? {
        var param: [String: Any] = [:]
        param["pool"] = self.pool
        param["group"] = self.group
        return param
    }
    
}
