//
//  File.swift
//  
//
//  Created by BigBoss on 10/8/20.
//

import Foundation
public struct BlockParameter: MidgardParameter {
    
    public var asset: String
    public var height: Int
    
    public init(asset: String, height: Int) {
        self.asset = asset
        self.height = height
    }
    
    public var path: String {
//        return "/api/testchange"
        return "/"
    }
    
    public var params: [String: Any]? {
        var params = ["pool" : asset];
        params["height"] = String(height)
        return params
    }
}
