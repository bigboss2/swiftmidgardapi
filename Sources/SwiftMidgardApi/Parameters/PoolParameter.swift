//
//  File.swift
//  
//
//  Created by Bigboss on 9/12/20.
//

import Foundation

public struct PoolParameter: MidgardParameter{
    public var pool: String
    public var path: String {
        return "/v2/midgard/pool"
    }
    public var params: [String: Any]? {
        return ["pool" : pool]
    }
    public init(pool: String){ self.pool = pool }
}

public struct PoolImageParameter: MidgardParameter{
    public var path: String {
        return "api/v2/pool/image"
    }
    
    public init(){
        
    }

}

