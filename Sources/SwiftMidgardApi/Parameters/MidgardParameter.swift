//
//  File.swift
//  
//
//  Created by BigBoss on 9/12/20.
//

import Foundation

public protocol MidgardParameter {
    var params: [String: Any]? { get }
    var path: String {get}
}

extension MidgardParameter{
    public var params: [String : Any]?{
        return nil
    }
}

public struct StakerPoolDataParameter: MidgardParameter {
    public var assets: [String]
    private var address: String
    
    public init(address: String, assets: [String]){
        self.assets = assets
        self.address = address
    }
    
    public var path: String{
        return "/v2/staker/pool"
    }
    
    public var params: [String : Any]? {
        var param: [String: Any] = [:]
        param["pools"] = assets.joined(separator: ",")
        param["address"] = self.address
        return param
    }
}

public struct StakedStateParameter: MidgardParameter {
    public var pool: String
    private var address: String
    
    public init(address: String, pool: String){
        self.pool = pool
        self.address = address
    }
    
    public var path: String{
        return "/v2/history/liquidity"
    }
    
    public var params: [String : Any]? {
        var param: [String: Any] = [:]
        param["pools"] = pool
        param["address"] = self.address
        return param
    }
}



public struct StakerDetailParameter: MidgardParameter {
    public var address: String
    public init(address: String){
        self.address = address
    }
    
    public var path: String {
        return "/v1/stakers/\(address)"
    }

}

public struct StakerParameter {
    public var path: String {
        return "/v1/stakers"
    }
}

public struct AssetsParameter: MidgardParameter {
    public var assets: [String]
    public init(assets: [String]){
        self.assets = assets
    }
    
    public var path: String {
        return "/v1/assets"
    }

    public var params: [String : Any]? {
        return ["asset": assets.joined(separator: ",")]
    }
}


public struct Txs: MidgardParameter {
    
    public var path: String {
        return "/v1/txs"
    }
    
    public var params: [String : Any]? {
        var param: [String: Any] = [:]
        param["offset"] = self.offset
        param["limit"] = self.limit
        if let address = self.address {
            param["address"] = address
        }
        if let txid = self.txid {
            param["txid"] = txid
        }
        
        if let asset = self.asset {
            param["asset"] = asset
        }
        
        if let type = self.type {
            param["type"] = type.joined(separator: ",")
        }
        return  param
    }
    
    public var address: String?
    public var txid: String?
    public var asset: [String]?
    public var type:[String]?
    public var offset: Int
    public var limit: Int
    
    public init(offset: Int, limit: Int, address: String? = nil, type: [String]? = nil){
        self.offset = offset
        self.limit = limit
        self.address = address
        self.type = type
    }
    
    
}

