//
//  File.swift
//  
//
//  Created by Bigboss on 29.08.21.
//

import Foundation

public struct BalanceParameter: MidgardParameter {
    
    public var address: String
    public var chain: Chain
    
    public init(address: String, chain: Chain) {
        self.address = address
        self.chain = chain
    }
    
    public var path: String {
        switch chain {
        case .thorchain:
            return "/bank/balances/\(address)"
        case .binance:
            return "api/v1/balances/\(address)"
        case .litecoin:
            return "/api/v2/get_address_balance/LTC/\(address)"
        case .bitcoin, .bitcoincash:
            return "/\(chain.symbol)/address/\(address)/balance"
        case .ethereum:
            return "/getAddressInfo/\(address)"
        default:
            return ""
        }
    }
    
    public var params: [String : Any]? {
        switch self.chain {
        case .ethereum:
            return ["apiKey": "EK-uB3Fv-ehKXfyN-Chujs"]
        default: return nil
        }
    }
}
