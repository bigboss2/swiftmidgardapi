//
//  File.swift
//  
//
//  Created by Bigboss on 18.04.21.
//

import Foundation

public struct PoolGroupsParameter: MidgardParameter {
    private var addresses: [String]
    private var pathType: PathType
    
    public init(addresses: [String], path: PathType){
        self.addresses = addresses
        self.pathType = path
    }
    
    public var path: String{
        return self.pathType.rawValue
    }
    
    public var params: [String : Any]? {
        
        let variable = (self.path == PathType.stakeUnit.rawValue) ? "groups" : "address"
        
        var param: [String: Any] = [:]
        param[variable] = addresses.map{$0.lowercased()}.joined(separator: ",")
        return param
    }
}

public enum PathType: String {
    case poolGroupList = "/api/v3/member/pooladdress"
    case stakedList = "/api/v3/member/poollist"
    case stakeUnit = "/api/v3/member/poolunits"
}
