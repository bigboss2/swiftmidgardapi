//
//  File.swift
//  
//
//  Created by BigBoss on 10/4/20.
//

import Foundation

public struct AggregateViewType {
    public enum ViewType: String {
        case daily, hourly
    }
    var viewType: ViewType
    var asset: String
    var from: String?
    var to: String?
    
    public init(type: ViewType, asset: String, from: String?, to: String?){
        self.asset = asset
        self.from = from
        self.to = to
        self.viewType = type
    }
}

public struct AggregateParameter: MidgardParameter {
    
    public enum DataType: String {
        case pool, price
    }
    
    public var dataType: DataType
    public var parameter: AggregateViewType
    
    
    public init(parameter: AggregateViewType, dataType: DataType) {
        self.parameter = parameter
        self.dataType = dataType
    }
    
    public var path: String {
        return "/\(dataType.rawValue)" + "/\(parameter.viewType.rawValue)"
    }
    
    public var params: [String: Any]? {
        var params = ["asset" : parameter.asset];
        if let from = parameter.from {
            params["from"] = from
        }
        
        if let to = parameter.to {
            params["to"] = to
        }
        
        return params
    }
}
