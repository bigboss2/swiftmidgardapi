import XCTest
@testable import SwiftMidgardApi

final class SwiftMidgardApiTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(SwiftMidgardApi().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
