import XCTest

import SwiftMidgardApiTests

var tests = [XCTestCaseEntry]()
tests += SwiftMidgardApiTests.allTests()
XCTMain(tests)
